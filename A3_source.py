# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 17:36:45 2018

@author: Mohamed
"""

import numpy as np
from sklearn.utils import shuffle
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
from bonnerlib2 import dfContour as dfC

from matplotlib import cm

# ------------------ Question 1 --------------------
####################################################


# Question 1(a)

# New function 
def new_func(x,y,z):
	return True

def genData(mu0,mu1,Sigma0,Sigma1,N):
    Xandt = np.zeros((2*N,3))
    xy0 = np.random.multivariate_normal(mu0, Sigma0, N)
    #print xy0
    xy0t = np.concatenate((xy0,np.zeros((1,N)).T),axis=1)
    xy1 = np.random.multivariate_normal(mu1,Sigma1,N)
    xy1t = np.concatenate((xy1,np.ones((1,N)).T),axis=1)
    
    Xt = shuffle(np.concatenate((xy0t,xy1t),axis=0))
    
    # returns X matrix, T array
    return Xt[:,[0,1]],Xt[:,2]
    
mu0 = (0,-1)
mu1 = (-1,1)
Sigma0 = np.array([[2.0,0.5],[0.5,1.0]])
Sigma1 = np.array([[1.0,-1.0],[-1.0,2.0]])

training_set = genData(mu0,mu1,Sigma0,Sigma1,1000)
test_set = genData(mu0,mu1,Sigma0,Sigma1,10000)


# Question 1(b)
clf = MLPClassifier(hidden_layer_sizes = (1,),solver='sgd',activation='tanh',
                    learning_rate_init=0.01,tol=10e-10,max_iter=10000)
clf.fit(training_set[0],training_set[1])

colormap = np.where(training_set[1]==0,'red','blue')

#Plot the x vs y points of matrix of 1000 points
plt.figure(1)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
plt.title("Question 1(b): Neural net with 1 hidden unit.")

dfC(clf)
plt.show()

# Question 1(c)
clfc = MLPClassifier(hidden_layer_sizes = (2,),solver='sgd',activation='tanh',
                    learning_rate_init=0.01,tol=10e-10,max_iter=10000)
clfc.fit(training_set[0],training_set[1])

#
print ""
print "----------------Question 1(c) ------------------------------"

plt.figure(2)
fits = []
accs = []
plt.title("Question 1(c): Neural net with 2 hidden units.")
for i in range(9):
    accuracy = clfc.score(test_set[0],test_set[1])
    accs += [accuracy]
    print "Test Accuracy #",i,":", accuracy
    plt.subplot(3,3,i+1)
    plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
    fits.append(clf.fit(training_set[0],training_set[1]))
    dfC(clfc)
plt.show()
    
bestFit = fits[np.argmax(np.asarray(accs))]
plt.figure(3)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(bestFit)
plt.title("Question 1(c):Best neural net with 2 hidden units.")
plt.show()

print "Best Test Accuracy", accs[np.argmax(np.asarray(accs))]

#
## Question 1(d)
#
clfd = MLPClassifier(hidden_layer_sizes = (3,),solver='sgd',activation='tanh',
                    learning_rate_init=0.01,tol=10e-10,max_iter=10000)
clfd.fit(training_set[0],training_set[1])

print ""
print "----------------Question 1(d) ------------------------------"

plt.figure(4)
fits = []
accs = []

for i in range(9):
    accuracy = clfd.score(test_set[0],test_set[1])
    accs += [accuracy]
    print "Test Accuracy #",i,":", accuracy
    plt.subplot(3,3,i+1)
    plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
    plt.axis('off')
    fits.append(clfd.fit(training_set[0],training_set[1]))
    dfC(clfd)
plt.suptitle("Question 1(d): Neural net with 3 hidden units.")
plt.show()
    
bestFitd = fits[np.argmax(np.asarray(accs))]
plt.figure(5)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(bestFitd)
plt.title("Question 1(d):Best neural net with 3 hidden units.")
plt.show()
#

print "Best Test Accuracy", accs[np.argmax(np.asarray(accs))]

#
## Question 1(e)
#
clfe = MLPClassifier(hidden_layer_sizes = (4,),solver='sgd',activation='tanh',
                    learning_rate_init=0.01,tol=10e-10,max_iter=10000)
clfe.fit(training_set[0],training_set[1])

print ""
print "----------------Question 1(e) ------------------------------"

plt.figure(5)
fits = []
accs = []

for i in range(9):
    accuracy = clfe.score(test_set[0],test_set[1])
    accs += [accuracy]
    print "Test Accuracy #",i,":", accuracy
    plt.subplot(3,3,i+1)
    plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
    fits.append(clfe.fit(training_set[0],training_set[1]))
    dfC(clfe)
    plt.axis('off')
plt.suptitle("Question 1(e): Neural net with 4 hidden units.")
    
bestFit = fits[np.argmax(np.asarray(accs))]
plt.figure(6)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(bestFit)
plt.title("Question 1(e):Best neural net with 4 hidden units.")
plt.show()
#

print "Best Test Accuracy", accs[np.argmax(np.asarray(accs))]


# Question 1(g)

print ""
print "----------------Question 1(g) ------------------------------"

#bestFitd #use this 
plt.figure(7)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(clfd)
#clfd

# Getting the weight vectprs
w1 = clfd.coefs_[0][:,0]
w2 = clfd.coefs_[0][:,1]
w3 = clfd.coefs_[0][:,2]
w0 = clfd.intercepts_[0]

w = clfd.coefs_[0]

xmin = -6
xmax = 5
def plotDB(w,w0):
    y1 = -(w0+w[0]*xmin)/w[1]
    y2 = -(w0+w[0]*xmax)/w[1]
    plt.plot([xmin,xmax],[y1,y2],':k')
    plt.ylim(-4,5)
    plt.xlim(-4,5)
    
plotDB(w,w0)

plt.title("Question 1(g):Decision with 3 hidden units.")
plt.show()

#print ""
#print "----------------Question 1(h) ------------------------------"
#
##bestFitd #use this 
plt.figure(8)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(clfc)
#clfd

# Getting the weight vectprs
w1 = clfc.coefs_[0][:,0]
w2 = clfc.coefs_[0][:,1]
w0 = clfc.intercepts_[0]

w = clfc.coefs_[0]

xmin = -6
xmax = 5
def plotDB(w,w0):
    y1 = -(w0+w[0]*xmin)/w[1]
    y2 = -(w0+w[0]*xmax)/w[1]
    plt.plot([xmin,xmax],[y1,y2],':k')
    plt.ylim(-4,5)
    plt.xlim(-4,5)
    
plotDB(w,w0)

plt.title("Question 1(h):Decision with 2 hidden units.")
plt.show()

print ""
print "----------------Question 1(i) ------------------------------"

##bestFitd #use this 
plt.figure(9)
plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
dfC(clfe)
#clfd

# Getting the weight vectprs
w1 = clfe.coefs_[0][:,0]
w2 = clfe.coefs_[0][:,1]
w0 = clfe.intercepts_[0]

w = clfe.coefs_[0]

xmin = -6
xmax = 5
def plotDB(w,w0):
    y1 = -(w0+w[0]*xmin)/w[1]
    y2 = -(w0+w[0]*xmax)/w[1]
    plt.plot([xmin,xmax],[y1,y2],':k')
    plt.ylim(-4,5)
    plt.xlim(-4,5)
    
plotDB(w,w0)

plt.title("Question 1(i):Decision with 4 hidden units.")
plt.show()

print ""
print "----------------Question 1(k) ------------------------------"

# ------------- 2i -------------------------
t = np.linspace(0,1,1000)
precision = []
recall = []

for i in t:
    o_prob = clfd.predict_proba(test_set[0])[:,1]
    predArray = np.where(i<o_prob,1,0)
    
    predPos = np.sum(predArray)
        
    trueP = np.add(predArray,test_set[1])
    truePcount = np.sum(np.where(trueP==2,1,0))
    trueNcount = np.sum(np.where(trueP==0,1,0))
    
    falseNeg = 20000-predPos-trueNcount
    if predPos == 0:
        precision += [1]
    else:
        precision += [float(truePcount)/predPos]
    recall += [float(truePcount)/(truePcount+falseNeg)]

    
plt.figure(10)
plt.plot(recall,precision)
plt.xlabel('recall')
plt.ylabel('precision')
plt.title("Question 1(k): Precision/recall curve")
plt.show()
    
print "area under curve", np.trapz(precision,x=recall)







# Question 3 (a)
training_set = genData(mu0,mu1,Sigma0,Sigma1,10000)
test_set = genData(mu0,mu1,Sigma0,Sigma1,10000)
colormap = np.where(training_set[1]==0,'red','blue')

V = clfd.coefs_[0]
vo = clfd.intercepts_[0]

W = clfd.coefs_[1]
wo = clfd.intercepts_[1][0] # real number

def sigmoid(x):
    sigm = 1. / (1. + np.exp(-x))
    return sigm

X = training_set[0]
o2 = clfd.predict_proba(X)[:,1]

def forward(X,V,vo,W,w0):
    u = np.matmul(X,V) + vo
    h = np.tanh(u)
    z = np.matmul(h,W) + w0
    o = sigmoid(z)
    
    
    return u,h,z,o

o1 = forward(X,V,vo,W,wo)[3]
final = np.sum(o1 - o2)**2

#
## Question 3 (b)

def MYdfContour(w,v,wo,vo):
    ax = plt.gca()
    # The extent of xy space
    x_min,x_max = ax.get_xlim()
    y_min,y_max = ax.get_ylim()
     
    # form a mesh/grid over xy space
    h = 0.02    # mesh granularity
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    mesh = np.c_[xx.ravel(),yy.ravel()]
    
    # evaluate the decision functrion at the grid points
    Z = forward(mesh,v,vo,w,wo)[-1] # *** MODIFIED ***
    
    # plot the contours of the decision function
    Z = Z.reshape(xx.shape)
    mylevels=np.linspace(0.0,1.0,11)
    ax.contourf(xx, yy, Z, levels=mylevels, cmap=cm.RdBu, alpha=0.5)
    
    # draw the decision boundary in solid black
    ax.contour(xx, yy, Z, levels=[0.5], colors='k', linestyles='solid')

def convert(array):
    new = np.where(array>=0.5,1,0)
    return new

def bgd(J,K,lrate):
    
    errors = []
    train_acc = []
    test_acc = []
    
    sigma = 1
    v = sigma*np.random.randn(2*J)
    v = np.reshape(v,(2,J))
    vo = np.zeros((J)) # bias term
    
    w = sigma*np.random.randn(J)
    w = np.reshape(w,(J,1))
    
    wo = np.random.rand(1,1)[0][0]
    # do forward pass
    # use o 
    # comp
    # 1 hot encode vectors
    # 
    
#    
    x = training_set[0]
    for i in range(K):
        #print "loop", i
        prop = forward(x,v,vo,w,wo)
        u = prop[0]
        h = prop[1]
        o = prop[3]
        o = np.concatenate(o)
        
        t = training_set[1]
#        print "o shape:", np.shape(o)
#        print "t shape:", np.shape(t)
#        print "h shape:", np.shape(h)
#        print "w shape:", np.shape(w)
#        print "mul ", np.shape(np.matmul((o-t).T,h))
#        print "matmul",np.matmul((o-t).T,h)
#        
#        print "old w",w
        
        if K % 10 == 0 and K !=0:
            #print "loss",np.sum(o - training_set[1])**2
            c_loss = -t*np.log(o)-(1-t)*np.log(1-o)
            #print "C Loss:",np.sum(c_loss)
            errors += [np.sum(c_loss)]
            y = convert(o) # convert o1s into binary 
            train_acc += [float(np.sum([np.where(y==t,1,0)]))/20000]
            
            xtest = test_set[0]
            ttest = test_set[1]
            testprop = forward(xtest,v,vo,w,wo)
            otest = testprop[3]
            otest = np.concatenate(otest)
            ytest = convert(otest)
            
            test_acc += [float(np.sum([np.where(ytest==ttest,1,0)]))/20000]
#        
        w = w - lrate*(np.matmul((o-t).T,h)).reshape(J,1)
        wo = wo - lrate*np.sum((o-t))
        
        #print "new w shape:", np.shape(w)
        
        #print "new w",w
        
        A = ((o-t).T)*x.T
        B = w.T/(np.cosh(u)**2)
        v = v - lrate*np.matmul(A,B)
        vo = vo - lrate*np.matmul((o-t),B)
        #print "end loop", i
        
        
            
            
            
#        record loss
   
    
    plt.figure(8)
    plt.semilogx(range(K),train_acc)
    plt.semilogx(range(K),test_acc)
    plt.xlabel("Epoch")
    plt.ylabel("Training and Test Accuracy")
    plt.title("Question 3 (b): training and test accuracy for bgd")
    
    plt.figure(9)
    plt.semilogx(range(K),errors)
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.title("Question 3(b): training loss for bgd")
    print "Final Training Accuracy: ",train_acc[-1]
    
    plt.figure(10)
    plt.plot(range(K/2),test_acc[K/2:])
    plt.xlabel("Epoch")
    plt.ylabel("Test Accuracy")
    plt.title("Question 3(b): final test accuracy for bgd")
    
    plt.figure(11)
    plt.plot(range(K/2),errors[K/2:])
    plt.xlabel("Epoch")
    plt.ylabel("Training Loss")
    plt.title("Question 3(b): final training loss for bgd")
    
    
    plt.figure(12)
    plt.scatter(training_set[0][:,0],training_set[0][:,1],c=colormap,s=2)
    MYdfContour(w,v,wo,vo)
    plt.title("Question 3(b): Decision boundary for my neural net")
    plt.show()

    
    
    
    return w,v,wo,vo
#    
#    

# 3 (C) I DONT KNOW
# 3 (D) I DONT KNOW



    
    